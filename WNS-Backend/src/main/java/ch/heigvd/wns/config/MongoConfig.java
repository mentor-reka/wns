package ch.heigvd.wns.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@EnableMongoRepositories("ch.heigvd.wns.repository.mongo")
public class MongoConfig extends AbstractMongoConfiguration {

    @Bean
    public Dotenv dotenv() {
        return Dotenv
                .configure()
                .ignoreIfMissing()
                .load();
    }

    @Override
    public MongoClient mongoClient() {
        String URI = dotenv().get("MONGO_URI");
        MongoClientURI mongoClientURI = new MongoClientURI(URI);
        return new MongoClient(mongoClientURI);
    }

    @Override
    protected String getDatabaseName() {
        return dotenv().get("DB_NAME");
    }

}
